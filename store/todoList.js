const state = () => {
  return {
    listData: [],
  }
}
const mutations = {
  setListData(state, payload) {
    state.listData = JSON.parse(payload)
  },
  udpateItem(state, payload) {
    let index = state.listData.findIndex((e) => e.id === payload.id)
    state.listData[index].title = payload.title
    state.listData[index].nameTask = payload.title
    state.listData[index].description = payload.description
    state.listData[index].dueDate = payload.dueDate
    state.listData[index].piority = payload.piority
    localStorage.removeItem('myData')
    localStorage.setItem('myData', JSON.stringify(state.listData))
  },
  setNewItem(state, payload) {
    state.listData.push({
      id: payload.id,
      title: payload.title,
      nameTask: payload.title,
      description: payload.description,
      dueDate: payload.dueDate,
      piority: payload.piority,
      titleError: [],
      dateError: [],
    })
    localStorage.removeItem('myData')
    localStorage.setItem('myData', JSON.stringify(state.listData))
  },
  deleteARecord(state, payload) {
    let index = state.listData.findIndex((e) => e.id === payload)
    state.listData.splice(index, 1)
    localStorage.removeItem('myData')
    localStorage.setItem('myData', JSON.stringify(state.listData))
  },
  removeAllRecord(state, payload) {
    for (let i = 0; i < payload.length; i++) {
      let index = state.listData.findIndex((e) => e.id === payload[i])
      state.listData.splice(index, 1)
    }
    localStorage.removeItem('myData')
    localStorage.setItem('myData', JSON.stringify(state.listData))
  },
}
const actions = {
  udpateItem(vueContext, payload) {
    vueContext.commit('udpateItem', payload)
  },
  insetItem(vueContext, payload) {
    vueContext.commit('setNewItem', payload)
  },
  removeAllRecord(vueContext, payload) {
    vueContext.commit('removeAllRecord', payload)
  },
}

export default {
  state,
  mutations,
  actions,
}
